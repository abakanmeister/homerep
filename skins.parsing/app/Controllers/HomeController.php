<?php


namespace App\Controllers;
use Slim\Views\Twig as View;

class HomeController extends Controller
{
    public function index($request, $response) {
        /**
         * Creating user data
         */
        /*User::create([
            'name' => 'Alex',
            'email' => 'alex@gmail.com',
            'password' => 'lel123',
        ]);*/
        return $this->view->render($response, 'home.twig');
    }
    public function trading($request, $response) {
        return $this->view->render($response, 'trading.twig');
    }
    public function bets($request, $response) {
        return $this->view->render($response, 'bets.twig');
    }
}